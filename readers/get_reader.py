import numpy as np
import simple_caching
from functools import partial
from neural_wrappers.readers import RandomIndexDatasetReader, MergeBatchedDatasetReader, \
	StaticBatchedDatasetReader, CachedDatasetReader

from .video_dir_reader import VideoDirReader
from .video_plus_context_dir_reader import VideoPlusContextDirReader
from .fine_tune_video_reader import FineTuneVideoReader
from .augmentation import AllAugmentationTransform

def getReader(dataCfg, trainCfg):
	np.random.seed(42)
	transform = AllAugmentationTransform(**dataCfg["augmentation_params"]) if dataCfg["useAugmentation"] else None
	if dataCfg["datasetType"] == "vox-256":
		reader = VideoDirReader(dataCfg["datasetPath"])
		mergeFn = partial(VideoDirReader.mergeFn, transform=transform)
		cacheDir = ".cache/vox-256/%d" % (len(reader.videos))
		buildCache = False
	elif dataCfg["datasetType"] == "vox-256-context":
		reader = VideoPlusContextDirReader(dataCfg["datasetPath"], context=dataCfg["context"])
		mergeFn = partial(VideoPlusContextDirReader.mergeFn, transform=transform)
		cacheDir = ".cache/vox-256-context/ctx%d/%d" % (dataCfg["context"], len(reader.videos))
		buildCache = False
	elif dataCfg["datasetType"] == "finetune-video":
		assert "datasetName" in dataCfg
		mergeFn = partial(FineTuneVideoReader.mergeFn, transform=transform)
		reader = FineTuneVideoReader(dataCfg["datasetPath"], targetFrameIx=dataCfg["targetFrameIx"])
		cacheDir = ".cache/%s" % dataCfg["datasetName"]
		buildCache = True
	else:
		assert False

	cache = simple_caching.NpyFS(cacheDir)
	reader = CachedDatasetReader(reader, cache=cache, buildCache=buildCache)
	reader = RandomIndexDatasetReader(reader, seed=42)
	reader = MergeBatchedDatasetReader(reader, mergeFn)
	reader = StaticBatchedDatasetReader(reader, batchSize=trainCfg["batch_size"])
	return reader
