import numpy as np
from neural_wrappers.readers import DatasetReader
from neural_wrappers.utilities import fullPath
from media_processing_lib.video import tryReadVideo
from overrides import overrides
from typing import List

class VideoDirReader(DatasetReader):
	def __init__(self, datasetPath:str, useFullVideo:bool=False, useSpecificFrames:List=None):
		super().__init__(
			dataBuckets={"data" : ["rgb"]},
			dimGetter={"rgb" : VideoDirReader.rgbGetter},
			dimTransform={}
		)

		self.datasetPath = datasetPath
		self.videos = [fullPath(x) for x in datasetPath.glob("*.mp4")]
		self.useFullVideo = useFullVideo
		self.useSpecificFrames = useSpecificFrames
		self.datasetFormat.isCacheable = True

	@staticmethod
	def mergeFn(x, transform):
		MB = len(x)
		H, W = x[0]["data"]["rgb"]["source"].shape[0 : 2]
		batchSource, batchDriving = np.zeros((2, MB, 3, H, W), dtype=np.float32)
		for i in range(MB):
			source = x[i]["data"]["rgb"]["source"]
			driving = x[i]["data"]["rgb"]["driving"]
			source = np.float32(source) / 255
			driving = np.float32(driving) / 255
			if not transform is None:
				source, driving = transform([source, driving])
			batchSource[i] = source.transpose(2, 0, 1)
			batchDriving[i] = driving.transpose(2, 0, 1)

		items = {
			"source" : batchSource,
			"driving" : batchDriving,
		}
		return items, items

	def rgbGetter(obj, index:int):
		path = str(obj.videos[index])
		video = tryReadVideo(path, vidLib="pims")

		assert obj.useFullVideo == False
		if not obj.useSpecificFrames is None:
			frame_idx = obj.useSpecificFrames
			if frame_idx[0] < 0:
				frame_idx[0] += len(video)
			if frame_idx[1] < 0:
				frame_idx[1] += len(video)
		else:
			np.random.seed(42)
			perm = np.random.permutation(len(video))
			frame_idx = np.random.permutation(len(video))
		frame_idx = min(frame_idx), max(frame_idx)
		frames = video[frame_idx]
		source, driving = frames
		return {"source" : source, "driving" : driving, "path" : path, "indexes" : frame_idx}

	@overrides
	def getDataset(self):
		return self

	@overrides
	def __len__(self) -> int:
		return len(self.videos)

	def __str__(self) -> str:
		Str = super().__str__()
		Str += "\n[VideoDirReader]"
		Str += "\n - Dataset path: %s" % self.datasetPath
		Str += "\n - Num videos: %d" % (len(self.videos))
		return Str