import numpy as np
from neural_wrappers.readers import DatasetReader
from .video_dir_reader import VideoDirReader

class VideoPlusContextDirReader(VideoDirReader):
	def __init__(self, datasetPath:str, context:int, useFullVideo:bool=False):
		super().__init__(datasetPath, useFullVideo)
		assert context > 1
		self.context = context
		DatasetReader.__init__(
			self,
			dataBuckets={"data" : ["rgb"]},
			dimGetter={"rgb" : VideoPlusContextDirReader.rgbGetter},
			dimTransform={}
		)
		self.datasetFormat.isCacheable = True

	@staticmethod
	def mergeFn(x, transform):
		MB = len(x)
		xDriving = x[0]["data"]["rgb"]["drivingContext"]
		context = len(xDriving)
		H, W = xDriving[0].shape[0 : 2]

		batchSource = np.zeros((MB, 3, H, W), dtype=np.float32)
		batchDriving = np.zeros((MB, 3, H, W), dtype=np.float32)
		batchDrivingContext = np.zeros((MB, context, 3, H, W), dtype=np.float32)
		for i in range(MB):
			source = x[i]["data"]["rgb"]["source"]
			driving = x[i]["data"]["rgb"]["driving"]
			drivingContext = x[i]["data"]["rgb"]["drivingContext"]
			source = np.float32(source) / 255
			driving = np.float32(driving) / 255
			drivingContext = np.float32(drivingContext) / 255
			if not transform is None:
				res = transform([source, *drivingContext, driving])
				source = res[0]
				driving = res[-1]
				drivingContext = np.array(res[1 : -1])
			batchSource[i] = source.transpose(2, 0, 1)
			batchDriving[i] = driving.transpose(2, 0, 1)
			batchDrivingContext[i] = drivingContext.transpose(0, 3, 1, 2)

		items = {
			"source" : batchSource,
			"driving" : batchDriving,
			"drivingContext" : batchDrivingContext
		}
		return items, items

	def rgbGetter(obj, index:int):
		path = str(obj.videos[index])
		video = tryReadVideo(path, vidLib="pims")

		np.random.seed(42)
		assert obj.useFullVideo == False

		N = len(video) - obj.context + 1
		perm = np.random.permutation(N) + obj.context - 1
		first, last = [perm[0], perm[1]] if perm[0] < perm[1] else [perm[1], perm[0]]
		drivingContext = [last + ix for ix in range(-obj.context, 0)]
		frame_idx = [first, *drivingContext, last]
		frames = video[frame_idx]
		source = frames[0]
		drivingContext = frames[1 : -1]
		driving = frames[-1]
		return {"source" : source, "driving" : driving, "drivingContext" : drivingContext, \
			"path" : path, "indexes" : frame_idx}

	def __str__(self) -> str:
		Str = super().__str__()
		Str += "\n[VideoPlusContextDirReader]"
		Str += "\n - Dataset path: %s" % self.datasetPath
		Str += "\n - Num videos: %d" % (len(self.videos))
		Str += "\n - Context: %d" % self.context
		return Str
