## Overfitting First-Order Model to a new video

Requirements: A relatively large video should be used (>= 5minutes @ 30 fps)

Command: `python main_crop_new_video.py --nTrain N --nTest M`

This will take a video called `video.mp4` and create two directories `train/` and `test/` with random samples of 2 seconds. The first 80% of the video is used for train set, while the last 20% is used for test set. It will also crop the video to be centered around the face. Only one person is expected. `nTrain` and `nTest` are used for the number of samples of two seconds to be extracted. The longer the video, the bigger the chance of pure randomness and non-repeating sequences.

The format will be:
 - videos: `train/1.mp4`, ..., `train/N.mp4`, `test/1.mp4`, ..., `test/M.mp4`
 - keypoints: `train/1.npy`, ..., `train/N.npy`, `test/1.npy`, ..., `test/M.npy`
