import numpy as np
from pathlib import Path
from argparse import ArgumentParser
from media_processing_lib.video import tryReadVideo, tryWriteVideo
from media_processing_lib.image import imgResize
from neural_wrappers.utilities import fullPath
from vke.face import vidGetFaceBbox, vidGetFaceKeypoints
from vke.utils import squareBbox, addPaddingToBbox, smoothBbox
from tqdm import trange

def getArgs():
    parser = ArgumentParser()
    parser.add_argument("--nTrain", type=int, required=True)
    parser.add_argument("--nTest", type=int, required=True)
    parser.add_argument("--videoPath", default="video.mp4")
    parser.add_argument("--sampleDuration", type=int, default=2)
    parser.add_argument("--bboxPaddingPercent", help="height%,width%", default="10,0")
    args = parser.parse_args()
    args.videoPath = fullPath(args.videoPath)
    args.bboxPaddingPercent = [float(x) for x in args.bboxPaddingPercent.split(",")]
    assert args.videoPath.exists(), "Does not exist: %s" % args.videoPath
    assert args.sampleDuration > 0
    assert len(args.bboxPaddingPercent) == 2
    return args

def faceAndResolutionAdjustedKPs(kps, bbox, resolution):
    x1, x2, y1, y2 = bbox
    # kps are from the original image. We have a bounding box around the face, so subtracting top_left corner should
    #  align the keypoints to the bounding box
    faceFrameKp = kps - [y1, x1]
    # Now, we'll also adjust the face-adjusted keypoints to the new resolution. Suppose the face bbox is (384x280) and
    #  our target resolution is (256x256). We will make a rapport for height (256/384) and width (256/280) and apply
    #  it to each keypoint. Finally, we convert to int, so they are valid coordinates.
    hFace, wFace = y2 - y1, x2 - x1
    hRes, wRes = resolution
    wRapp, hRapp = wRes / wFace, hRes / hFace
    resKps = faceFrameKp * [hRapp, wRapp]
    resKps = np.clip(resKps, [0, 0], [hRes - 1, wRes - 1])
    assert (resKps < 0).sum() == 0
    assert (resKps[:, 0] >= hRes).sum() == 0
    assert (resKps[:, 1] >= wRes).sum() == 0
    resKps = np.uint32(resKps)
    return resKps

def doSample(frames, bbox, kps, vidPath, kpsPath, resolution, fps):
    # frames = np.array(frames)
    height, width = resolution
    N = len(frames)

    resultFrames = np.zeros((N, height, width, 3), dtype=np.uint8)
    resultKps = np.zeros(kps.shape, dtype=kps.dtype)
    for i in range(N):
        frame = frames[i]
        x1, x2, y1, y2 = bbox[i]
        faceFrame = frame[y1:y2, x1:x2]
        resizedFaceFrame = imgResize(faceFrame, height=height, width=width, resizeLib="lycon")
        faceKp = faceAndResolutionAdjustedKPs(kps[i], bbox[i], resolution)

        resultFrames[i] = resizedFaceFrame
        resultKps[i] = faceKp
    np.save(kpsPath, resultKps)
    tryWriteVideo(resultFrames, vidPath, fps=fps)

def doVideo(video, bbox, kps, N, sampleDuration, Dir):
    # 2 seconds hardcoded (for now?)
    # duration = 2 * video.fps
    assert len(video) > sampleDuration, ("[doVideo] Cannot export samples. Video duration must be at least " + \
        "as long as sample duration. Got %d vs %d" % (len(video), sampleDuration))
    startingFrames = np.random.choice(len(video) - sampleDuration, size=N, replace=True)
    Path(Dir).mkdir(exist_ok=False, parents=True)

    for i in trange(N):
        startIx, endIx = startingFrames[i], startingFrames[i] + sampleDuration
        frames = video[startIx : endIx]
        framesBbox = bbox[startIx : endIx]
        framesKPs = kps[startIx : endIx]
        outFileVideo = "%s/%d.mp4" % (Dir, i)
        outFileKps = outFileVideo.replace("mp4", "npy")
        doSample(frames, framesBbox, framesKPs, outFileVideo, outFileKps, resolution=(256, 256), fps=video.fps)

def getFaceBbox(video, paddingPercent):
    def vidBboxAvgRatio(vidBbox):
        x1, x2, y1, y2 = vidBbox.T
        return (x2 - x1).mean(), (y2 - y1).mean()

    def padBbox(video, vidBbox, percent):
        H, W = video.shape[1 : 3]
        yPercent, xPercent = percent
        # Nx4 => 4xN
        vidBbox = vidBbox.T
        x1, x2, y1, y2 = vidBbox
        # Get the left and right padding
        yValue = np.int32(yPercent * (y2 - y1) / 100)
        xValue = np.int32(xPercent * (x2 - x1) / 100)
        # Apply the padding and clip outside of video values
        vidBbox = vidBbox + [-xValue, xValue, -yValue, yValue]
        vidBbox = vidBbox.T
        vidBbox = np.clip(vidBbox, [0, 0, 0, 0], [W, W, H, H])
        # 4xN => Nx4
        return vidBbox

    faceBbox = vidGetFaceBbox(video, faceLib="face_alignment")
    print("[getFaceBbox] Initial bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    faceBbox = smoothBbox(faceBbox)
    print("[getFaceBbox] Smoothed bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    faceBbox = padBbox(video, faceBbox, paddingPercent)
    print("[getFaceBbox] Padded (H:%2.2f%%, W:%2.2f%%) bbox avg ratios (H, W): %s" % \
        (paddingPercent[0], paddingPercent[1], str(vidBboxAvgRatio(faceBbox))))
    faceBbox = np.array([squareBbox(faceBbox[i]) for i in range(len(faceBbox))])
    print("[getFaceBbox] Squared bbox avg ratios (H, W): %s" % str(vidBboxAvgRatio(faceBbox)))
    return np.int32(faceBbox)

def main():
    args = getArgs()

    video = tryReadVideo(args.videoPath, vidLib="pims")
    args.sampleDuration = int(np.ceil(args.sampleDuration * video.fps))
    print("[main] Read video: %s. Length: %d. FPS: %2.2f." % (args.videoPath, len(video), video.fps))

    print("[main] Extracting face bbox.")
    faceBbox = getFaceBbox(video, args.bboxPaddingPercent)
    print("[main] Face bbox shape: %s" % str(faceBbox.shape))

    print("[main] Extracting face keypoints.")
    faceKPs = vidGetFaceKeypoints(video, faceLib="face_alignment")
    print("[main] Face keypoints shape: %s" % str(faceKPs.shape))

    nTrain = int(0.8 * len(video))
    videoTrain, videoTest = video[0 : nTrain], video[nTrain: ]
    faceBboxTrain, faceBboxTest = faceBbox[0 : nTrain], faceBbox[nTrain :]
    faceKPsTrain, faceKPsTest = faceKPs[0 : nTrain], faceKPs[nTrain :]
    print("[main] Using %d frames for train set and %d for test set." % (len(videoTrain), len(videoTest)))

    print("[main] Exporting train set...")
    doVideo(videoTrain, faceBboxTrain, faceKPsTrain, args.nTrain, args.sampleDuration, Dir="train")
    print("[main] Done.")

    print("[main] Exporting test set...")
    doVideo(videoTest, faceBboxTest, faceKPsTest, args.nTest, args.sampleDuration, Dir="test")
    print("[main] Done.")

if __name__ == "__main__":
    main()