import numpy as np
import torch.optim as optim
from neural_wrappers.callbacks import SaveModels, SaveHistory, PlotMetrics, RandomPlotEachEpoch
from neural_wrappers.utilities import minMax
from neural_wrappers.pytorch.utils import _getOptimizerStr
from .first_order_original import FirstOrder as FirstOrderOriginal
from .first_order_plusplus import FirstOrderPlusPlusV1
from media_processing_lib.image import toImage, tryWriteImage
from matplotlib.cm import hot

def metricL1Warp(y, t, **kwargs):
	y = np.int32(y["generated"]["prediction"] * 255)
	t = np.int32(t["driving"] * 255)
	diffs = np.abs(y - t)
	return diffs.mean()

def plotFn(x, y, t):
	MB = len(x["source"])
	source = np.array([toImage(x["source"][i]) for i in range(MB)])
	driving = np.array([toImage(x["driving"][i]) for i in range(MB)])
	generated = np.array([toImage(y["generated"]["prediction"][i]) for i in range(MB)])
	diffs = np.abs(generated - driving).sum(axis=-1)
	diffs = np.array([toImage(hot(minMax(diffs[i]))) for i in range(MB)])
	stack = np.concatenate([source, driving, generated, diffs], axis=2)
	for i in range(MB):
		tryWriteImage(stack[i], "%d.png" % i)

class MyOptimizer(optim.Optimizer):
	def __init__(self, params, **kwargs):
		self.storedArgs = kwargs
		self.trainCfg = self.storedArgs["trainCfg"]

		self.optimizers = {
			"generator" : optim.Adam(params["generator"], lr=self.trainCfg["lr_generator"], betas=(0.5, 0.999)),
			"discriminator" : optim.Adam(params["discriminator"], \
				lr=self.trainCfg["lr_discriminator"], betas=(0.5, 0.999)),
			"kp_detector" : optim.Adam(params["kp_detector"], lr=self.trainCfg["lr_kp_detector"], betas=(0.5, 0.999)),
		}

	def state_dict(self):
		return {k : self.optimizers[k].state_dict() for k in self.optimizers}

	def load_state_dict(self, state):
		for k in self.optimizers:
			self.optimizers[k].load_state_dict(state[k])

	def __str__(self):
		_str = _getOptimizerStr(self.optimizers["generator"])[0].split(".")[0]
		Str = "First Order Optimizer [%s]" % (_str)
		return Str

def getModel(modelCfg, trainCfg):
	if modelCfg["modelType"] == "first-order-original":
		model = FirstOrderOriginal(modelCfg, trainCfg)
	elif modelCfg["modelType"] == "first-order++-v1":
		model = FirstOrderPlusPlusV1(modelCfg, trainCfg)
	else:
		assert False, "Unknown model type: %s" % modelCfg["modelType"]

	# if tr.cuda.is_available():
	# 	model.generator_full = DataParallelWithCallback(model.generator_full, device_ids=args.deviceIDs)
	# 	model.discriminator_full = DataParallelWithCallback(model.discriminator_full, device_ids=args.deviceIDs)
	model.setOptimizer(MyOptimizer({
		"generator" : model.generator.parameters(),
		"discriminator" : model.discriminator.parameters(),
		"kp_detector" : model.kp_detector.parameters()
	}, trainCfg=trainCfg))
	callbacks = [SaveModels("best", "Loss"), SaveModels("last", "Loss"), \
		SaveHistory("history.txt"), PlotMetrics(["Loss", "L1 warp"]), RandomPlotEachEpoch(plotFn)]
	model.addCallbacks(callbacks)
	model.addMetrics({"L1 warp" : metricL1Warp})

	return model
